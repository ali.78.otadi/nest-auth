import { Entity, Column } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Role } from '../types/role.enum';

@Entity({ name: 'user' })
export class User extends BaseEntity {
  @Column({ type: 'varchar', length: 300, unique: true })
  username: string;

  @Column({ type: 'varchar', length: 300 })
  password: string;

  @Column({ type: 'varchar', length: 300, default: Role.Customer })
  role: Role;

  constructor(username?: string, password?: string, role?: Role) {
    super();

    this.username = username;
    this.password = password;
    this.role = role;
  }
}
