import { IsString } from 'class-validator';

import { User } from '../models/user.entity';

export class UserRegisterDto {
  @IsString()
  username: string;

  @IsString()
  password: string;

  public static from(dto: Partial<UserRegisterDto>) {
    const userRegisterDto = new UserRegisterDto();
    userRegisterDto.username = dto.username;
    userRegisterDto.password = dto.password;
    return userRegisterDto;
  }

  public static fromEntity(entity: Partial<User>) {
    return this.from({
      username: entity.username,
      password: entity.password,
    });
  }

  public toEntity(): User {
    const user = new User();
    user.username = this.username;
    user.password = this.password;
    return user;
  }
}
