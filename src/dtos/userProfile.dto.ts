import { IsString, IsEnum } from 'class-validator';

import { User } from '../models/user.entity';
import { Role } from '../types/role.enum';

export class UserProfileDto implements Readonly<UserProfileDto> {
  @IsString()
  username: string;

  @IsEnum(Role)
  role: Role;

  public static from(dto: Partial<UserProfileDto>) {
    const profileDto = new UserProfileDto();
    profileDto.username = dto.username;
    profileDto.role = dto.role;
    return profileDto;
  }

  public static fromEntity(entity: Partial<User>) {
    return this.from({
      username: entity.username,
      role: entity.role,
    });
  }

  public toEntity(): User {
    const user = new User();
    user.username = this.username;
    user.role = this.role;
    return user;
  }
}
