import { Role } from './role.enum';

export class UserAccount {
  username: string;
  password?: string;
  role: Role;

  constructor(username: string, password: string, role: Role = Role.Customer) {
    this.username = username;
    this.password = password;
    this.role = role;
  }
}
