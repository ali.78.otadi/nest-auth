export enum Role {
  Customer = 'customer',
  Agent = 'agent',
  Admin = 'admin',
  SU = 'su',
}
