import * as Joi from 'joi';

import { Role } from '../types/role.enum';

export const setRoleSchema = Joi.object({
  username: Joi.string().required(),
  role: Joi.string()
    .valid(...Object.values(Role))
    .required(),
});

export const registerSchema = Joi.object({
  username: Joi.string().trim().min(3).required(),
  password: Joi.string().trim().min(3).required(),
});
