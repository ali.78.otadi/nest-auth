import {
  Controller,
  Post,
  Get,
  Patch,
  Request,
  Body,
  UseGuards,
  // UsePipes,
} from '@nestjs/common';

import { UserService } from '../services/user.service';
import { LocalAuthGuard } from '../configs/auth/localAuthGuard';
import { JwtAuthGuard } from '../configs/auth/jwtAuthGuard';
import { UserRegisterDto } from '../dtos/userRegister.dto';
import { UserProfileDto } from '../dtos/userProfile.dto';
import { Roles } from '../decorators/roles.decorator';
import { Role } from '../types/role.enum';
import { RolesGuard } from '../guards/roles.guard';

@Controller('/user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/register')
  // @UsePipes(new JoiValidationPipe(registerSchema))
  async register(@Body() body: UserRegisterDto): Promise<UserProfileDto> {
    const dto = UserRegisterDto.from(body);

    return this.userService.userRegister(dto);
  }

  @UseGuards(LocalAuthGuard)
  @Post('/login')
  async login(@Request() req) {
    return this.userService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/profile')
  getProfile(@Request() req) {
    return req.user;
  }

  @Patch('/set-role')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.SU)
  // @UsePipes(new JoiValidationPipe(setRoleSchema))
  setRule(@Body() body: UserProfileDto): Promise<UserProfileDto> {
    const dto = UserProfileDto.from(body);
    return this.userService.setRole(dto);
  }
}
