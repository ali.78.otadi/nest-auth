import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';

import { configService } from './configs/config.service';

import { AppModule } from './modules/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(configService.getPort());
}

bootstrap();
