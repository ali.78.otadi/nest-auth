import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';

import { UserAccount } from '../types/userAccount';
import { Role } from '../types/role.enum';
import { User } from '../models/user.entity';
import { UserProfileDto } from '../dtos/userProfile.dto';
import { UserRegisterDto } from '../dtos/userRegister.dto';

@Injectable()
export class UserService {
  constructor(
    private jwtService: JwtService,
    @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) {}

  async userRegister(
    userRegisterDto: UserRegisterDto,
  ): Promise<UserProfileDto> {
    const userEntity = userRegisterDto.toEntity();

    try {
      await this.userRepo.save(userEntity);
    } catch (e) {
      if (e.code === '23505') {
        throw new HttpException('username already exists', HttpStatus.CONFLICT);
      }
      throw new HttpException('error', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return UserProfileDto.fromEntity(userEntity);
  }

  async login(user: UserAccount) {
    const payload = { username: user.username, role: user.role };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async findUser(username: string): Promise<User | undefined> {
    return this.userRepo.findOne({ username });
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user: User = await this.findUser(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async setRole(dto: UserProfileDto): Promise<UserProfileDto> {
    const userEntity = dto.toEntity();
    const user = await this.findUser(userEntity.username);

    if (user) {
      user.role = dto.role;
      await this.userRepo.update(user.id, user);
      return UserProfileDto.fromEntity(user);
    } else throw new HttpException('not found', HttpStatus.NOT_FOUND);
  }
}
