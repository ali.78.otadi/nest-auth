import {MigrationInterface, QueryRunner} from "typeorm";

export class init1628508209953 implements MigrationInterface {
    name = 'init1628508209953'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "createdBy" character varying(300) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "lastChangedBy" character varying(300) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "lastChangedBy"`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "createdBy"`);
    }

}
